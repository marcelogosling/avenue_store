
function Product(item){
    var self = this;
    self.id = ko.observable(item.id);
    self.name = ko.observable(item.name);
    self.description = ko.observable(item.description);
    self.parent = ko.observable(item.parent);
    self.children = ko.observableArray(item.children);
    self.images = ko.observableArray(item.images);
}

function ProductViewModel() {
    var self = this;
    self.base_url = "/products/"
    self.products = ko.observableArray([]);
    self.show_children = ko.observable(false)
    self.show_images = ko.observable(false)
    self.get_data = function (self){
        $.getJSON(makeUrl(self), function(data) {
            var mappedProducts = $.map(data, function(item) { return new Product(item) });
            self.products(mappedProducts);
        });
    }
    function makeUrl(self) {
        if(self.show_images() && self.show_children()){return self.base_url+'?images&children';}
        if(self.show_images()){return self.base_url+'?images';}
        if(self.show_children()){return self.base_url+'?children';}
        return self.base_url
    }

    self.get_data(self);
}

ko.applyBindings(new ProductViewModel());
