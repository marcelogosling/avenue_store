#Installing and running

I suggest making a virtualenv beforehand

```
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```
the server will run on http://127.0.0.1:8000 by default



#API Endpoints

| Endpoint  | Verb  | descrption |
|-|-|-|
|/products/| GET | product list|
|/products/ | POST | upload products in JSON |
|/products/?images | GET | product list with images |
|/products/?children | GET | product list with children |
|/products/?images&children | GET | product list with images and children |
|/products/1 | GET | product information on product 1 |
|/products/1 | PUT | change product information on product 1 |
|/products/1 | DELETE | delete product 1 |
|/products/1?images | GET | product information on product 1 with images |
|/products/1?children | GET | product information on product 1 with children |
|/products/1?images&children | GET | product information on product 1 with images and children |
|/products/1/images/ | GET | just product 1 images |
|/products/1/children/ | GET | just product 1 children |
