from rest_framework import serializers
from .models import Product, Image

class ProductSerializer(serializers.ModelSerializer):
    parent = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'parent', 'children', 'images')

    def __init__(self, *args, **kwargs):
        super(ProductSerializer, self).__init__(*args, **kwargs)
        if 'children' not in self.context['request'].GET:
            self.fields.pop('children')
        if 'images' not in self.context['request'].GET:
            self.fields.pop('images')


class ProductChildrenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('children',)


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('images',)


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'product', )
